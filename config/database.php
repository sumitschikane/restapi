<?php
class Database{
 
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "restApi";
    private $username = "root";
    private $password = "";
    public $conn;
    // get the database connection
    public function getConnection(){
 
        $this->conn = null;
 
        try{
        /**Add DB connection here**/
            $this->conn = new PDO();
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }

}
?>